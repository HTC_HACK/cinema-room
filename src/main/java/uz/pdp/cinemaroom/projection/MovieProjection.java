package uz.pdp.cinemaroom.projection;


//Asadbek Xalimjonov 15/03/22 22:37

import uz.pdp.cinemaroom.entity.cinema.Hall;

import java.util.List;
import java.util.UUID;

public interface MovieProjection {

    UUID getId();

    String getMovieTitle();

    UUID getCoverImageId();

    List<String> getHalls();


}
