package uz.pdp.cinemaroom.projection;


//Asadbek Xalimjonov 18/03/22 15:42

public interface SeatProjection {

    String getId();

    Integer getNumber();

    String getType();

    Double getPrice();

    String getHall();

}
