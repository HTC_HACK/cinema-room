package uz.pdp.cinemaroom.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.PageRequest;

@AllArgsConstructor
@NoArgsConstructor
@Data

//Asadbek Xalimjonov 18/03/22 15:04

public class SeatDto {

    private Integer startNumber;
    private Integer endNumber;
    private String price_category_id;
}
