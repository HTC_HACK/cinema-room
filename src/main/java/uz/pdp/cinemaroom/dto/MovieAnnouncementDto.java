package uz.pdp.cinemaroom.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data

//Asadbek Xalimjonov 16/03/22 10:25

public class MovieAnnouncementDto {
    private Boolean active;
    private String movieId;
}
