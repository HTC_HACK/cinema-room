package uz.pdp.cinemaroom.service.search;

import org.springframework.stereotype.Repository;
import uz.pdp.cinemaroom.entity.movie.Movie;


//Asatbek Xalimojnov 4/8/22 4:11 PM

@Repository
public interface MovieRepository extends SearchRepository<Movie, String> {

}
