package uz.pdp.cinemaroom.repository;

//Asadbek Xalimjonov 15/03/22 12:13


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.pdp.cinemaroom.entity.movie.Director;

@Repository
public interface DirectorRepo extends JpaRepository<Director, String> {
}
