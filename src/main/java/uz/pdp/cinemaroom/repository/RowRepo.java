package uz.pdp.cinemaroom.repository;


//Asadbek Xalimjonov 18/03/22 14:45

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.pdp.cinemaroom.entity.cinema.Row;

@Repository
public interface RowRepo extends JpaRepository<Row, String> {
}
