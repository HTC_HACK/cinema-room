package uz.pdp.cinemaroom.repository;


//Asadbek Xalimjonov 01/04/22 09:40

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.cinemaroom.entity.paytype.WaitingPurchaseTime;

public interface WaitingPurchaseTimeRepo extends JpaRepository<WaitingPurchaseTime, String> {
}
