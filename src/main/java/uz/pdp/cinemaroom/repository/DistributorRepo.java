package uz.pdp.cinemaroom.repository;

//Asadbek Xalimjonov 14/03/22 21:02

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.pdp.cinemaroom.entity.movie.Distributor;

@Repository
public interface DistributorRepo extends JpaRepository<Distributor, String> {

}
