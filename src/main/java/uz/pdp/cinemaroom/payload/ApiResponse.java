package uz.pdp.cinemaroom.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data

//Asadbek Xalimjonov 14/03/22 21:17

public class ApiResponse {

    private boolean status;
    private String message;
    private Object data;

}
