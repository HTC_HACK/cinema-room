package uz.pdp.cinemaroom;

//Asatbek Xalimojnov 4/8/22 4:41 PM


import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import uz.pdp.cinemaroom.service.search.SearchRepositoryImpl;

@Configuration
@EnableJpaRepositories(repositoryBaseClass = SearchRepositoryImpl.class)
public class ApplicationConfiguration {
}

