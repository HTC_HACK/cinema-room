package uz.pdp.cinemaroom.entity.movie;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.web.multipart.MultipartFile;
import uz.pdp.cinemaroom.entity.absEntity.AbsEntity;
import uz.pdp.cinemaroom.entity.attachment.Attachment;
import uz.pdp.cinemaroom.entity.user.UserDetail;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Data

//Asadbek Xalimjonov 14/03/22 19:02


@Entity(name = "directors")
@OnDelete(action = OnDeleteAction.CASCADE)
public class Director extends AbsEntity {

    private String firstName;
    private String lastName;


    @OneToOne(cascade = CascadeType.REMOVE)
    private Attachment attachment;
}
