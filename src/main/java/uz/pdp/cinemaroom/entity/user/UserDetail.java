package uz.pdp.cinemaroom.entity.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;

@AllArgsConstructor
@NoArgsConstructor
@Data

//Asadbek Xalimjonov 14/03/22 12:42

@Embeddable
public class UserDetail {

    private String firstName;
    private String lastName;

}
